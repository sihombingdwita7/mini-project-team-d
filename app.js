const express = require("express");
const app = express();
var session = require("express-session");
const flash = require('express-flash');
const {PORT = 3000} = process.env



//setting request body parser
//(Body parser harus ditempatkan paling atas)
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Setting Express session
app.use(
    session({
        secret: "secret",
        resave: true,
        saveUninitialized: true,
    })
);

//Setting passport
//(ditempatkan sebelum router dan view engine)
var passport = require("./lib/passport");
app.use(passport.initialize());             //passport config
app.use(passport.session());                //passport middleware


//Setting flash
app.use(flash());

//Setting view engine
app.use(express.static("public"));
app.set("view engine", "ejs");
const path = require("path");
app.set("views", path.join(__dirname, "views"));

//check error
app.use((err, req, res, next) => {
    if (err) {
        console.log(err);
    }
});

//Setting router
const router = require("./routes");
app.use(router)
app.listen(PORT, () => {
    console.log(`Example app listening at http://localhost:${PORT}`);
});

module.exports = app;