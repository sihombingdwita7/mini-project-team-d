const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const bcrypt = require("bcrypt");

const { Teacher, Parent } = require('../models')

// function authenticateTeacher(email, password, done) {
//     Teacher.findOne({
//             where: {
//                 email: email,
//             },
//         })
//         .then((teacher) => {
//             console.log(teacher);
//             if (teacher && teacher.dataValues) {
//                 const encryptedPassword = teacher.dataValues.password

//                 if (bcrypt.compareSync(password, encryptedPassword)) {
//                     return done(null, teacher)
//                 } else {
//                     return done(null, false)
//                 }
//             } else {
//                 return done(null, false)
//             }
//         });
// }

// function authenticateParent(email, password, done) {
//     Parent.findOne({
//             where: {
//                 email: email,
//             },
//         })
//         .then((parent) => {
//             console.log(parent);
//             if (parent && parent.dataValues) {
//                 const encryptedPassword = parent.dataValues.password

//                 if (bcrypt.compareSync(password, encryptedPassword)) {
//                     return done(null, parent)
//                 } else {
//                     return done(null, false)
//                 }
//             } else {
//                 return done(null, false)
//             }
//         });
// }

async function authenticateTeacher(email, password, done){
    try {
        // Memanggil method kita tadi
        const teacher = await Teacher.authenticate({ email, password })

        return done(null, teacher)
    } catch (err) {
        return done(null, false, { message: err })
    }
}

async function authenticateParent(email, password, done){
    try {
        // Memanggil method kita tadi
        const parent = await Parent.authenticate({ email, password })

        return done(null, parent)
    } catch (err) {
        return done(null, false, { message: err })
    }
}

passport.use('teacher',
    new LocalStrategy({
            usernameField: "email",
            passwordField: "password",
        },
        authenticateTeacher
    )
);

passport.use('parent',
    new LocalStrategy({
            usernameField: "email",
            passwordField: "password",
        },
        authenticateParent   
    )
);


passport.serializeUser((teacher, done) => {
    done(null, teacher);
});
passport.deserializeUser((obj, done) => {
    done(null, obj);
});

module.exports = passport
