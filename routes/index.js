const router = require("express").Router();

const parentRouter = require('./parentRouter');
const teacherRouter = require('./teacherRouter');


const controller = require('../controllers/index')
const order = require('../controllers/orderController')


router.use('/parent', parentRouter);
router.use('/teacher', teacherRouter)

router.get('/', (req, res) => res.render('index'))

router.get('/register', (req, res) => res.render('register'))
router.post('/register', controller.register)

router.post('/createOrder', order.createOrder)




module.exports = router;