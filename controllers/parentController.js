const {
    Parent,
    Teacher,
    Order
} = require('../models');
const passport = require('../lib/passport');
// const teacher = require("./teacherController")

module.exports = {
    register: (req, res, next) => {
        Parent.register(req.body).then(() => {
            res.redirect('login')
        }).catch(err => next(err))
    },

    login: passport.authenticate('parent', {
        successRedirect: '/parent/dashboard',
        failureRedirect: '/parent/login',
        failureFlash: true
    }),

    logout: (req, res) => {
        req.logout();
        res.redirect('/');
    },

    dashboard: (req, res) => {
        const currentUser = req.user.nama;
        const posisi = req.user.role;
        Order.findAll({
            where: {
                ParentId: req.user.id
            },
            include: [{
                model: Teacher
            }],
            order: [
                ['id', 'DESC'],
            ]
        }).then(orders => {

            res.render('dashboard_parent', {
                orders
            })
        }).catch(err => next(err))

    },
    //menampilkan semua parents
    allParent: (req, res) => {
        Parent.findAll({
            order: [
                ['id', 'ASC'],
            ],
        }).then(parents => {
            // res.json({
            //     'status': 200,
            //     'data' : parents
            // })
            // res.render('user/inbox', {title, parent})
            const currentUser = req.user.nama;
            res.render('dashboard_parent', {
                currentUser,
                parents
            })
        }).catch(err => next(err))
    },

    detail: (req, res) => {
        const id = req.params.id
        Parent.findOne({
            where: {
                id: id
            }
        }).then(parent => {
            res.render('parent/edit_parent', { parent })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    },
    update: (req, res) => {
        const id = req.params.id
        Parent.update({
            nama: req.body.nama,
            email: req.body.email,
            provinsi: req.body.provinsi,
            kabupaten: req.body.kabupaten,
            kecamatan: req.body.kecamatan,
            kelurahan: req.body.kelurahan,
            alamat: req.body.alamat,
        }, {
            where: {
                id: id
            }
        }).then(parent => {
            res.send('/parent/dashboard')
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    },

    delete: (req, res) => {
        const id = req.params.id;
        Parent.destroy({
            where: {
                id: id
            }
        }).then(() => {
            res.json({
                'status': 200,
                'message': 'success deleting'
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    },

    edit: (req, res) => {
        const id = req.user.id
        Parent.findOne({
            where: {
                id: id
            }
        }).then(parent => {
            res.render('parent/edit_parent', {parent})
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
             })
        })
    },

    getEksplorePage: (req, res) => {
        Teacher.findAll({
            order: [
                ['id', 'ASC'],
            ],
        }).then(teachers => {
            // console.log(teachers.dataValues)
            res.render("./parent/explore_teacher", {
                teachers: teachers
            })
        }).catch(err => next(err))

    },


    getTeacherDetail: (req, res) => {
        const id = req.params.id
        Teacher.findOne({
            where: {
                id: id
            }
        }).then(teacher => {
            res.render("./parent/detail_teacher", {
                teacher
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    }

}